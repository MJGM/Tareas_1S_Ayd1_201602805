# TAREA NO. 2
## María Julissa García Morán - 201602805
# REQUERIMIENTOS FUNCIONALES Y NO FUNCIONALES DE “https://mcdonalds.com.gt/”

## 10 REQUERIMIENTOS FUNCIONALES
```
1. Visualizar el menú.
2. Registro de usuario.
3. Poder realizar pedidos de manera personalizada.
4. Promociones y descuentos dentro de la página para despertar interés.
5. Opción de pago.
6. Visualizar el seguimiento del pedido.
7. Elegir el idioma de la página.
8. Soporte al cliente dentro de la página.
9. Información de las ubicaciones de los restaurantes.
10. Manejo o gestión de la cuenta.
```
## 10 REQUERIMIENTOS NO FUNCIONALES
```
1. Rendimiento, basándose en las expectativas de velocidad y capacidad de la página.
2. Seguridad para proteger dicha página y los datos obtenidos por medio de ella.
3. Escalabilidad, en donde se maneja grandes cantidades de usuarios.
4. Actualizaciones de la página.
5. Mantenimiento de la página para garantizar su buen funcionamiento.
6. Fiabilidad, confiando que la página funcionará de la forma esperada.
7. Diseño responsivo, la página se pueda adaptar en cualquier dispositivo en donde se utilice.
8. Intuitiva, diseñada para que sea de fácil uso.
9. Disponibilidad, que la página esté disponible las 24hrs para poder acceder a ella en cualquier momento.
10. Cumplir con el normativo establecido en el país.
```
