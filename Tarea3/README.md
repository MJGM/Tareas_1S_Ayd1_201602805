# Tarea No. 3
Tarea No. 3 del Laboratorio de Análisis y Diseño 1
# Documentación
* María Julissa García Morán - 201602805
## Herramientas Necesarias
 * Visual Studio Code
 * Python
 * Flask
 * Docker

## Despliegue en Docker
### Crear imagen en Docker:
 ```
 docker build -t tarea3 .
 ```
### Ejecutar el contenedor en Docker:
 ```
 docker run -d -p 3000:3000 -e RUN_HOST=0.0.0.0 tarea3
 ```

# Ejecución del contenedor
![ejecucion](Ejecucion.png)
# Servidor corriendo en Docker
 ![servidorDocker](Docker.png)
# Llamada y Respuesta del Servidor
![respuestaServidor](ResultadoGET.png)
![respuestaDocker](SalidaDocker.png)
