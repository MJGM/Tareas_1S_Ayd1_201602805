from flask import Flask


app = Flask(__name__)

@app.route('/Tarea3_201602805', methods=['GET'])
def mostrar_info():
    return{
        'Nombre': 'María Julissa García Morán',
        'Carnet': '201602805',
        'Curso': 'Análisis y Diseño 1',
        'Mejor_Auxiliar': 'Bryan Gerardo Paez Morales'
    }
    
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=3000)