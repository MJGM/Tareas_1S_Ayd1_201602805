const express = require('express');
const app = express();
const port = 4000;

// Endpoint para /holamundo
app.get('/holamundo', (req, res) => {
    res.send('Hola Mundo');
  });

// Endpoint para /nombre
app.get('/nombre', (req, res) => {
    res.send('Maria Julissa Garcia Moran');
  });

// Endpoint para /carnet
app.get('/carnet', (req, res) => {
    res.send('201602805');
  });

app.listen(port, () => {
    console.log('Servidor corriendo en puerto localhost:4000')
});