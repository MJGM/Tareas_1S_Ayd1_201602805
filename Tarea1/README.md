# TAREA 1
## MARÍA JULISSA GARCÍA MORÁN
## 201602805

## v-1.0.0 - GET /holamundo
### RESPUESTA SERVIDOR
![Respuesta](Tarea1/ENDPOINT_holamundo.png)

### FLUJO EN GITKRAKEN
![FlujoGit](Tarea1/GITKRAKEN_1.png)

## v-2.0.0 - GET /nombre
### RESPUESTA SERVIDOR
![Respuesta](Tarea1/ENDPOINT_nombre.png)

### FLUJO EN GITKRAKEN
![FlujoGit](Tarea1/GITKRAKEN_2.png)

## v-3.0.0 - GET /carnet
### RESPUESTA SERVIDOR
![Respuesta](Tarea1/ENDPOINT_carnet.png)

### FLUJO EN GITKRAKEN
![FlujoGit](Tarea1/GITKRAKEN_3.png)

